#!/usr/bin/perl
package Helpers;

{
package TestWebServer;

use HTTP::Server::Simple::CGI;
use base qw(HTTP::Server::Simple::CGI);

my %dispatch = (
    '/hello' => \&resp_hello,
    # ...
);

sub handle_request {
    my $self = shift;
    my $cgi  = shift;
  
    my $path = $cgi->path_info();
    my $handler = $dispatch{$path};

    if (ref($handler) eq "CODE") {
        print "HTTP/1.0 200 OK\r\n";
        $handler->($cgi);
        
    } else {
        print "HTTP/1.0 404 Not found\r\n";
        print $cgi->header,
              $cgi->start_html('Not found'),
              $cgi->h1('Not found'),
              $cgi->end_html;
    }
}

sub resp_hello {
    my $cgi  = shift;   # CGI.pm object
    return if !ref $cgi;
     
    print $cgi->header,
          $cgi->start_html("hello"),
          $cgi->h1("hello"),
          $cgi->end_html;
}
 
} # package TestWebServer

use LWP::UserAgent;
use HTTP::Request;
use FindBin qw/ $Bin /;

use POSIX ":sys_wait_h";
$SIG{CHLD} = sub {
    while ((my $child = waitpid(-1, WNOHANG)) > 0) {
        print("child $child status: $?\n");
    }
};

use constant UPSTREAM_HOST => "127.0.0.1";
use constant UPSTREAM_PORT => 8080;

use constant PROXY_BIN => "$Bin/../tcp_proxy";
use constant DEFAULT_PROXY_HOST => "127.0.0.1";
use constant DEFAULT_PROXY_PORT => 80;

our $srv;
our $srv_pid;

our $proxy_pid;
our $proxy_config;
 
# return 0 on success, 1 on failure
sub start_server {
    eval {
        $srv = TestWebServer->new(UPSTREAM_PORT);
        $srv->host(UPSTREAM_HOST);
        $srv_pid = $srv->background();
        return 1 unless(kill(0 => $srv_pid));
        return 0;
    };
    return 1 if $@;
}

# return 0 on success, 1 on failure
sub stop_server {
    my $sig = shift;
    unless($sig) {
        $sig = 'SIGTERM';
    }
    kill($sig => $srv_pid);
    for(my $i = 0; $i < 3; $i++) {
        if(0 == kill(0 => $srv_pid)) {
            return 0; # process was killed
        }
        # process is still alive, waiting
        sleep 1;
    }
    return 1;
}

sub get_host_and_port {
    my $where = shift;
    my ($host, $port);
    if($where && $where eq 'proxy') {
        $host = exists $proxy_config->{proxy_host} ? $proxy_config->{proxy_host} : DEFAULT_PROXY_HOST;
        $port = exists $proxy_config->{proxy_port} ? $proxy_config->{proxy_port} : DEFAULT_PROXY_PORT;
    }
    else { # send to upstream
        $host = $srv->host();
        $port = $srv->port();
    }
    return ($host, $port);
}

# return 0 on success, 1 on failure
sub send_valid_request {
    my $where = shift;
    my ($host, $port) = get_host_and_port($where);
    my $req = HTTP::Request->new(GET => "http://$host:$port/hello");
    my $ua = LWP::UserAgent->new();
    my $resp = $ua->request($req);
    return $resp->is_success ? 0 : 1;
}

# return 0 on success, 1 on failure
sub send_invalid_request {
    my $where = shift;
    my ($host, $port) = get_host_and_port($where);
    my $req = HTTP::Request->new(GET => "http://$host:$port/NON_EXISTENT_PATH");
    my $ua = LWP::UserAgent->new();
    my $resp = $ua->request($req);
    return $resp->is_error ? 0 : 1;
}

# return 0 on success, 1 on failure
sub start_proxy {

    my $config = shift;
    my $proxy_cmd = PROXY_BIN;

    if($config) {
        $proxy_config = $config;
        $proxy_cmd .= " -p $config->{proxy_port}" if exists $config->{proxy_port};
        $proxy_cmd .= " -h $config->{proxy_host}" if exists $config->{proxy_host};
        $proxy_cmd .= " -H $config->{upstream_host}" if exists $config->{upstream_host};
        $proxy_cmd .= " -P $config->{upstream_port}" if exists $config->{upstream_port};
        $proxy_cmd .= " -w $config->{workers}" if exists $config->{workers};
        $proxy_cmd .= " -c $config->{config_file}" if exists $config->{config_file};
    }

    my $pid = fork();
    if($pid) { # parent
        $proxy_pid = $pid;
        # make sure our child is alive for at least 1 second
        sleep 1;
        return 0 if kill(0 => $proxy_pid);
        # oops, looks like child is dead. cleanup and return error
        undef $proxy_pid;
        return 1;
    }
    elsif(defined($pid)) { # child
        exec $proxy_cmd or return 1;
        return 0;
    }
    # fork failed
    print "Failed to fork: $!\n";
    return 1;
}

# return 1 if proxy is alive, 0 otherwise
sub proxy_alive {
    return 0 unless $proxy_pid;
    return kill(0 => $proxy_pid);
}

# return 0 on success, 1 on failure
sub stop_proxy {
    return 0 unless $proxy_pid;
    my $sig = shift;
    unless($sig) {
        $sig = 'SIGTERM';
    }
    kill($sig => $proxy_pid);
    for(my $i = 0; $i < 3; $i++) {
        if(0 == kill(0 => $proxy_pid)) {
            return 0; # process was killed
        }
        # process is still alive, waiting
        sleep 1;
    }
    return 1;
}

1;
