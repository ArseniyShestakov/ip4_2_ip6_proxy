/* vim: set tabstop=4 shiftwidth=4 expandtab : */
#ifndef MANAGE_MONGO_DB_H_
#define MANAGE_MONGO_DB_H_

#include <libmongoc-1.0/mongoc.h>

#define COLLECTION_DB_USERS "users"

int check_user(mongoc_client_pool_t* pool, const char* mongo_db, const char* user, const char* pass);

#endif /* MANAGE_MONGO_DB_H_ */
