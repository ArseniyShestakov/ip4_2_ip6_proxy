/* vim: set tabstop=4 shiftwidth=4 expandtab : */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <libconfig.h>

#include "defs.h"
#include "util.h"
#include "config.h"

static int parse_port_range(tcp_proxy_config_t *, char *, int);
static const char config_shm_name[] = "tcp_proxy_config";

static char config_file[PATH_MAX] = {0};

static int shm_fd = -1;

static void *config_ptr = NULL;

int alloc_config() {
    int res = 0; 
    shm_fd = shm_open(config_shm_name, O_CREAT|O_RDWR, 0666);
    if(-1 == shm_fd) {
        pr_err("Failed to create shared memory object: %s", strerror(errno));
        res = -1;
        goto cleanup;
    }

    if(-1 == ftruncate(shm_fd, sizeof(tcp_proxy_config_t))) {
        pr_err("Failed to set shared memory object size: %s", strerror(errno));
        res = -1;
        goto cleanup;
    }

    config_ptr = mmap(0, sizeof(tcp_proxy_config_t), PROT_READ|PROT_WRITE, MAP_SHARED, shm_fd, 0);
    if(MAP_FAILED == config_ptr) {
        pr_err("Failed to map config object to shared memory: %s", strerror(errno));
        res = -1;
        goto cleanup;
    }

cleanup:
    if(-1 == res && -1 != shm_fd) {
        shm_unlink(config_shm_name);
    }
    return res;
}

int open_config() {
    int res = 0; 
    shm_fd = shm_open(config_shm_name, O_RDONLY, 0666);
    if(-1 == shm_fd) {
        pr_err("Failed to create shared memory object: %s", strerror(errno));
        res = -1;
        goto cleanup;
    }

    config_ptr = mmap(0, sizeof(tcp_proxy_config_t), PROT_READ, MAP_SHARED, shm_fd, 0);
    if(MAP_FAILED == config_ptr) {
        pr_err("Failed to map config object to shared memory: %s", strerror(errno));
        res = -1;
        goto cleanup;
    }

cleanup:
    if(-1 == res) {
        free_config();
    }
    return res;
}

void free_config() {
    if(NULL != config_ptr) {
        munmap(config_ptr, sizeof(tcp_proxy_config_t));
    }
    if(-1 != shm_fd) {
        shm_unlink(config_shm_name);
    }
}

tcp_proxy_config_t *get_config() {
    CHECK(NULL != config_ptr);
    return (tcp_proxy_config_t *)config_ptr;
}

const char *get_config_filename() {
    return config_file;
}

static void usage() {
    pr_err("Usage: %s [options]\n"
           "options:\n"
           "\t-c file\t-\tpath to config file (other options will be ignored)\n"
           "\t-h host\t-\tlisten host address (default: %s)\n"
           "\t-p port\t-\tlisten host ports range in format start-end (default: %ld)\n"
           "\t-d host\t-\tdhcpd host address (default: %s)\n"
           "\t-D port\t-\tdhcpd host port (default: %ld)\n"
           "\t-t seconds\t-\tclient timeout (default: %ld)\n"
           "\t-T seconds\t-\tupstream timeout (default: %ld)\n"
           "\t-a seconds\t-\tbind addr change timeout (default: %ld)\n"
           "\t-w integer\t-\tnumber of worker processes, 0 - num of cpus (default: %ld)\n"
           "\t-i string\t-\tname of outgoing interface, (default: %s). For multiple use config file\n",
           "tcp_proxy",
           DEFAULT_PROXY_HOST, DEFAULT_PROXY_PORT,
           DEFAULT_DHCP_HOST, DEFAULT_DHCP_PORT,
           DEFAULT_PROXY_CLIENT_TIMEOUT, DEFAULT_PROXY_UPSTREAM_TIMEOUT, DEFAULT_BIND_ADDR_CHANGE_TIMEOUT,
           DEFAULT_PROXY_NUM_WORKERS, DEFAULT_PROXY_OUT_IF);
}

void set_config_defaults() {
    tcp_proxy_config_t *cfg = get_config();
    CHECK(NULL != cfg);

    strncpy(cfg->host, DEFAULT_PROXY_HOST, PROXY_HOST_LEN);
    cfg->port_range = NULL;
    cfg->port_range = (long *) realloc(cfg->port_range, sizeof(long));
    *cfg->port_range = DEFAULT_PROXY_PORT;

    strncpy(cfg->dhcpd_host, DEFAULT_DHCP_HOST, PROXY_HOST_LEN);
    cfg->dhcpd_port = DEFAULT_DHCP_PORT;
    cfg->dhcpd_sock_family = AF_INET;

    cfg->upstream_timeout = DEFAULT_PROXY_UPSTREAM_TIMEOUT;
    cfg->parent_timeout = DEFAULT_PARENT_TIMEOUT;
    cfg->client_timeout = DEFAULT_PROXY_CLIENT_TIMEOUT;
    cfg->addr_change_timeout = DEFAULT_BIND_ADDR_CHANGE_TIMEOUT;

    cfg->num_workers = DEFAULT_PROXY_NUM_WORKERS;

    strncpy(cfg->mongo_uri, DEFAULT_MONGO_URI, MONGO_URI_LEN);
    strncpy(cfg->mongo_db, DEFAULT_MONGO_DB, MONGO_DB_LEN);

    cfg->out_ifs = xcalloc(1, sizeof(char *));
    cfg->out_ifs[0] = DEFAULT_PROXY_OUT_IF;
    cfg->num_ifs = 1;
    cfg->daemon_mode =1;
    cfg->socks5_auth =0;
}

int parse_command_line(int argc, char **argv) {
    int opt = 0, ports_count = 0;

    set_config_defaults();
    tcp_proxy_config_t *cfg = get_config();
    ASSERT(NULL != cfg);

    while((opt = getopt(argc, argv, "nc:h:p:d:D:t:T:a:w:i:")) != -1) {
        switch(opt) {
        case 'c':
            strncpy(config_file, optarg, PATH_MAX);
            /* config file found, nothing to do here further */
            return 0;
        case 'h':
            strncpy(cfg->host, optarg, PROXY_HOST_LEN);
            break;
        case 'p':
            ports_count = parse_port_range(cfg, optarg, ports_count);
            break;
        case 'd':
            strncpy(cfg->dhcpd_host, optarg, PROXY_HOST_LEN);
            cfg->dhcpd_sock_family = strstr(cfg->dhcpd_host, ".sock") ? AF_UNIX : AF_INET;
            break;
        case 'D':
            cfg->dhcpd_port = atoi(optarg);
            break;
        case 't':
            cfg->client_timeout = atoi(optarg);
            break;
        case 'T':
            cfg->upstream_timeout = atoi(optarg);
            break;
        case 'a':
            cfg->addr_change_timeout = atoi(optarg);
            break;
        case 'w':
            cfg->num_workers = atoi(optarg);
            break;
        case 'i':
            cfg->num_ifs = 1;
            cfg->out_ifs = (char **)xcalloc(1, sizeof(char *));
            cfg->out_ifs[0] = (char *)xmalloc(strlen(optarg + 1));
            strcpy(cfg->out_ifs[0], optarg);
            break;
        case 'n':
            cfg->daemon_mode = 0;
            break;
        default:
            usage();
            return -1;
        }
    }

    return 0;
}

int parse_config_file(const char *filename) {
    int res = 0, ports_error = 0;
    config_t cfg;
    const char *str;
    config_setting_t *port_list;
    config_setting_t *out_ifs;

    tcp_proxy_config_t *proxy_config = get_config();
    ASSERT(NULL != proxy_config);

    config_init(&cfg);

    if(!config_read_file(&cfg, filename)) {
        pr_err("%s %d - %s\n", config_error_file(&cfg),
            config_error_line(&cfg), config_error_text(&cfg));
        res = -1;
        goto cleanup;
    }

    set_config_defaults();

    if(CONFIG_FALSE == config_lookup_string(&cfg, "host", &str)) {
        pr_debug("host param not found, using default: '%s'", DEFAULT_PROXY_HOST);
    }
    else {
        strncpy(proxy_config->host, str, PROXY_HOST_LEN);
    }

    port_list = config_lookup(&cfg, "port_range");
    if (CONFIG_FALSE != port_list)
    {
        int i, ports_count = 0;
        int range_count = config_setting_length(port_list);

        for(i = 0; i < range_count; ++i)
        {
            config_setting_t *port_range = config_setting_get_elem(port_list, i);

            long start = 0, end = 0;

            if (!(config_setting_lookup_int(port_range, "start", &start) &&
                  config_setting_lookup_int(port_range, "end", &end)))
            {
                ports_error = 1;
                pr_err("port range parse error, start or end key not found");
                break;
            }
            if (!start || !end) {
                ports_error = 1;
                pr_err("Failed to parse ports: start '%ld' end '%ld'", start, end);
                break;
            }
            if (start > end)
            {
                ports_error = 1;
                pr_err("port range parse error, start must be lower then end");
                break;
            }

            proxy_config->port_range = (long *) xrealloc(proxy_config->port_range,
                                       sizeof(long) * (((end - start) + 1) + ports_count));
            for (; start <= end; ++start, ++ports_count)
            {
                proxy_config->port_range[ports_count] = start;
            }
        }
        if (ports_error)
        {
            proxy_config->port_range = realloc(proxy_config->port_range, sizeof(long));
            *proxy_config->port_range = DEFAULT_PROXY_PORT;
            pr_debug("port range parse error, using default: %d", DEFAULT_PROXY_PORT);
        }
        proxy_config->ports_count = ports_count;
    }
    else
    {
        pr_debug("port_range param not found, using default: %d", DEFAULT_PROXY_PORT);
        proxy_config->ports_count = 1;
    }

    if(CONFIG_FALSE == config_lookup_string(&cfg, "dhcpd_host", &str)) {
        pr_debug("dhcpd_host param not found, using default: '%s'", DEFAULT_DHCP_HOST);
    }
    else {
        strncpy(proxy_config->dhcpd_host, str, PROXY_HOST_LEN);
        proxy_config->dhcpd_sock_family = strstr(str, ".sock") ? AF_UNIX : AF_INET;
    }

    if(CONFIG_FALSE == config_lookup_int(&cfg, "dhcpd_port", &proxy_config->dhcpd_port)) {
        pr_debug("dhcpd_port param not found, using default: %d", DEFAULT_DHCP_PORT);
    }

    if(CONFIG_FALSE == config_lookup_int(&cfg, "upstream_timeout", &proxy_config->upstream_timeout)) {
        pr_debug("upstream_timeout param not found, using default: %d", DEFAULT_PROXY_UPSTREAM_TIMEOUT);
    }

    if(CONFIG_FALSE == config_lookup_int(&cfg, "parent_timeout", &proxy_config->parent_timeout)) {
        pr_debug("parent_timeout param not found, using default: %d", DEFAULT_PARENT_TIMEOUT);
    }

    if(CONFIG_FALSE == config_lookup_int(&cfg, "client_timeout", &proxy_config->client_timeout)) {
        pr_debug("client_timeout param not found, using default: %d", DEFAULT_PROXY_CLIENT_TIMEOUT);
    }

    if(CONFIG_FALSE == config_lookup_int(&cfg, "bind_addr_change_timeout", &proxy_config->addr_change_timeout)) {
        pr_debug("bind_addr_change_timeout param not found, using default: %ld", DEFAULT_BIND_ADDR_CHANGE_TIMEOUT);
    }

    if(CONFIG_FALSE == config_lookup_int(&cfg, "workers", &proxy_config->num_workers)) {
        pr_debug("workers param not found, using default: %d", DEFAULT_PROXY_NUM_WORKERS);
    }

    if (CONFIG_FALSE == config_lookup_bool(&cfg, "daemon_mode", &proxy_config->daemon_mode)) {
        pr_debug("daemon mode param not found, run as daemon by default");
    }

    if (CONFIG_FALSE == config_lookup_bool(&cfg, "socks5_auth", &proxy_config->socks5_auth)) {
        pr_debug("socks5 auth param not found, using noauth by default");
    }

    out_ifs = config_lookup(&cfg, "out_ifaces");
    if(CONFIG_FALSE != out_ifs) {
        size_t count = config_setting_length(out_ifs);
        pr_debug("Found %zu interfaces", count);
        proxy_config->num_ifs = count;
        proxy_config->out_ifs = xcalloc(count, sizeof(char *));
        for(size_t i = 0; i < count; ++i) {
            const char *s = config_setting_get_string_elem(out_ifs, i);
            if(!s || !(*s)) {
                pr_warn("Empty string in iface list (or can't get string). idx: %zu", i);
                proxy_config->num_ifs--;
                continue;
            }
            pr_debug("Got iface: '%s'", s);
            proxy_config->out_ifs[i] = strdup(s);
        }
    }
    else {
        pr_debug("Out iface list not found. Using default iface: '%s'", DEFAULT_PROXY_OUT_IF);
    }

    if(CONFIG_FALSE == config_lookup_string( &cfg, "mongodb_uri", &str )) {
        pr_debug("mongodb_uri param not found, using default: %s", DEFAULT_MONGO_URI);
    } else {
        strncpy( proxy_config->mongo_uri, str, MONGO_URI_LEN );
    }

    if(CONFIG_FALSE == config_lookup_string( &cfg, "mongodb_name", &str )) {
        pr_debug("mongo_db param not found, using default: %s", DEFAULT_MONGO_DB);
    } else {
        strncpy( proxy_config->mongo_db, str, MONGO_DB_LEN );
    }

cleanup:
    config_destroy(&cfg);
    return res;
}

void dump_config() {
    const tcp_proxy_config_t *config = get_config();
    if(!config) {
        return;
    }
    pr_debug("\nConfiguration:\nhost: %s port_start: %ld port_end: %ld\n"
             "dhcpd_host: %s dhcpd_port: %ld\n"
             "upstream_timeout: %ld parent_timeout: %ld client_timeout: %ld addr_change_timeout: %ld\n"
             "workers: %ld run_as_daemon: %s socks5_auth: %s\n"
             "mongodb_uri: %s\nmongodb_name: %s", config->host, config->port_range[0],
             config->port_range[config->ports_count - 1],
             config->dhcpd_host, config->dhcpd_port,
             config->upstream_timeout, config->parent_timeout, config->client_timeout, config->addr_change_timeout,
             config->num_workers, config->daemon_mode ? "yes" : "no", config->socks5_auth ? "yes" : "no", config->mongo_uri, config->mongo_db);
    for(size_t i = 0; i < config->num_ifs; ++i) {
        pr_debug("Found outgoing interface: '%s'", config->out_ifs[i]);
    }
}

static int parse_port_range(tcp_proxy_config_t *cfg, char *s, int count)
{
    long start, end;
    char *token, *saveptr;

    token = strtok_r(s, "-", &saveptr);
    if (token == NULL)
    {
        pr_err("incorrect port range value, must be port-port");
        return count;
    }
    start = atol(token);

    token = strtok_r(NULL, "-", &saveptr);
    if (token == NULL)
    {
        pr_err("incorrect port range value, must be port-port");
        return count;
    }
    end = atol(token);
    pr_debug("Ports: start %ld, end %ld", start, end);

    if (start > end)
    {
        pr_err("port range parse error, start must be lower then end");
        return count;
    }

    cfg->port_range = (long *) realloc(cfg->port_range,
                      sizeof(long) * (((end - start) + 1) + count));
    for (; start <= end; ++start, ++count)
    {
        cfg->port_range[count] = start;
    }

    cfg->ports_count = count;

    return count;
}
