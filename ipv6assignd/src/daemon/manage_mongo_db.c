/* vim: set tabstop=4 shiftwidth=4 expandtab : */
#include <stdio.h>
#include <syslog.h>
#include <pthread.h>
#include <arpa/inet.h>

#include <bson.h>
#include <json.h>

#include <manage_mongo_db.h>
#include <ipv6_calc.h>

static int counter = 1;
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

static int insert_ip_range( const char *, const char *, mongoc_collection_t *, bson_error_t * );
static int modify_ip_range( const char *, const char *, mongoc_collection_t *, bson_error_t *, int );

int generate_database( const daemon_data_t *data )
{
    mongoc_client_pool_t *pool = data->pool;
    mongoc_client_t *client;
    mongoc_collection_t *collection_s, *collection_w;
    bson_error_t error;
    bson_t *doc;

    struct in6_addr ip_begin, ip_end;
    char ip_str_start[INET6_ADDRSTRLEN], ip_str_end[INET6_ADDRSTRLEN];
    int res = 0;

    memset(&ip_begin, 0, sizeof(ip_begin));
    memset(&ip_end, 0, sizeof(ip_end));
    memset(ip_str_start, 0, sizeof(ip_str_start));
    memset(ip_str_end, 0, sizeof(ip_str_end));

    client = mongoc_client_pool_pop( pool );
    if( !client )
    {
        syslog( LOG_ERR, "Connection pooling failed" );
        return 1;
    }
    collection_s = mongoc_client_get_collection( client, data->cfg->mongo_db, COLLECTION_DB_START );
    collection_w = mongoc_client_get_collection( client, data->cfg->mongo_db, COLLECTION_DB_WORK );

    /* cleanup collections before insert */
    doc = bson_new();
    if( !mongoc_collection_remove( collection_s, MONGOC_REMOVE_SINGLE_REMOVE, doc, NULL, &error ) ||
        !mongoc_collection_remove( collection_w, MONGOC_REMOVE_SINGLE_REMOVE, doc, NULL, &error ))
    {
        res = 1;
        goto cleanup;
    }

    /* insert values in collections */
    ip_begin = data->cfg->ip_range.ip_start;
    ip_end = data->cfg->ip_range.ip_end;
    if( !inet_ntop( AF_INET6, &ip_begin, ip_str_start, INET6_ADDRSTRLEN ))
    {
        syslog( LOG_ERR, "%s", strerror( errno ));
        res = 1;
        goto cleanup;
    }
    if( !inet_ntop( AF_INET6, &ip_end, ip_str_end, INET6_ADDRSTRLEN ))
    {
        syslog( LOG_ERR, "%s", strerror( errno ));
        res = 1;
        goto cleanup;
    }
    if( insert_ip_range( ip_str_start, ip_str_end, collection_s, &error ) != 0 ||
        insert_ip_range( ip_str_start, ip_str_end, collection_w, &error ) != 0 )
    {
        syslog( LOG_ERR, "%s", error.message );
        res = 1;
        goto cleanup;
    }

cleanup:
    bson_destroy( doc );
    mongoc_collection_destroy( collection_s );
    mongoc_collection_destroy( collection_w );
    mongoc_client_pool_push( pool, client );

    return res;
}

void reload_database( const daemon_data_t *data )
{
    mongoc_client_pool_t *pool = data->pool;
    mongoc_client_t *client;
    mongoc_collection_t *collection_s;
    mongoc_cursor_t *cursor;
    const bson_t *doc;
    bson_t *query;
    json_object *jobj_root, *jobj_get;
    char *str;
    const char *ip_str_start_s, *ip_str_end_s;
    struct in6_addr ip_begin, ip_end;
    char ip_str_start_n[INET6_ADDRSTRLEN], ip_str_end_n[INET6_ADDRSTRLEN];

    memset(&ip_begin, 0, sizeof(ip_begin));
    memset(&ip_end, 0, sizeof(ip_end));
    memset(ip_str_start_n, 0, sizeof(ip_str_start_n));
    memset(ip_str_end_n, 0, sizeof(ip_str_end_n));

    client = mongoc_client_pool_pop( pool );
    if( !client )
    {
        syslog( LOG_ERR, "Connection pooling failed" );
        return;
    }

    ip_begin = data->cfg->ip_range.ip_start;
    ip_end = data->cfg->ip_range.ip_end;
    if( !inet_ntop( AF_INET6, &ip_begin, ip_str_start_n, INET6_ADDRSTRLEN ))
    {
        syslog( LOG_ERR, "%s", strerror( errno ));
        goto cleanup;
    }
    if( !inet_ntop( AF_INET6, &ip_end, ip_str_end_n, INET6_ADDRSTRLEN ))
    {
        syslog( LOG_ERR, "%s", strerror( errno ));
        goto cleanup;
    }

    collection_s = mongoc_client_get_collection( client, data->cfg->mongo_db, COLLECTION_DB_START );
    query = bson_new();

    cursor = mongoc_collection_find( collection_s, MONGOC_QUERY_NONE, 0, 0, 0, query, NULL, NULL );
    if ( !mongoc_cursor_next( cursor, &doc ))
    {
        mongoc_cursor_destroy( cursor );
        syslog( LOG_ERR, "No work_range records found" );
        goto cleanup;
    }

    str = bson_as_json( doc, NULL );
    jobj_root = json_tokener_parse( str );
    bson_free( str );
    mongoc_cursor_destroy( cursor );
    jobj_get = json_object_object_get( jobj_root, "start" );
    ip_str_start_s = json_object_get_string( jobj_get );
    jobj_get = json_object_object_get( jobj_root, "end" );
    ip_str_end_s = json_object_get_string( jobj_get );

    if( strncmp( ip_str_start_s, ip_str_start_n, INET6_ADDRSTRLEN ) == 0 &&
        strncmp( ip_str_end_s, ip_str_end_n, INET6_ADDRSTRLEN ) == 0 )
    {
        syslog( LOG_INFO, "IP ranges match, no need to reload database" );
    }
    else
    {
        syslog( LOG_INFO, "IP ranges mismatch, reloading database" );
        generate_database( data );
    }

    json_object_put( jobj_root );

cleanup:
    bson_destroy( query );
    mongoc_collection_destroy( collection_s );
    mongoc_client_pool_push( pool, client );
}

int get_available_address( const daemon_data_t *data, struct in6_addr *addr )
{
    mongoc_client_pool_t *pool = data->pool;
    mongoc_client_t *client;
    mongoc_collection_t *collection_w;
    mongoc_cursor_t *cursor;
    const bson_t *doc;
    bson_t *query;
    bson_error_t error;
    json_object *jobj_root, *jobj_get;
    char *str;
    const char *ip_str_start, *ip_str_end;
    int res, take_from_end, rc;

    res = take_from_end = 0;

    client = mongoc_client_pool_pop( pool );
    if( !client )
    {
        syslog( LOG_ERR, "Connection pooling failed" );
        return 1;
    }

    pthread_mutex_lock( &lock );
    if( counter <= data->cfg->percent )
    {
        take_from_end = 1;
    }
    if( counter == 100 )
    {
        counter = 1;
    }
    else
    {
        counter++;
    }
    pthread_mutex_unlock( &lock );

    collection_w = mongoc_client_get_collection( client, data->cfg->mongo_db, COLLECTION_DB_WORK );
    query = bson_new();

    cursor = mongoc_collection_find( collection_w, MONGOC_QUERY_NONE, 0, 0, 0, query, NULL, NULL );
    if ( !mongoc_cursor_next( cursor, &doc ))
    {
        mongoc_cursor_destroy( cursor );
        res = 1;
        syslog( LOG_ERR, "No work_range records found" );
        goto cleanup;
    }

    str = bson_as_json( doc, NULL );
    jobj_root = json_tokener_parse( str );
    bson_free( str );
    mongoc_cursor_destroy( cursor );
    jobj_get = json_object_object_get( jobj_root, "start" );
    ip_str_start = json_object_get_string( jobj_get );
    jobj_get = json_object_object_get( jobj_root, "end" );
    ip_str_end = json_object_get_string( jobj_get );
    inet_pton( AF_INET6, take_from_end ? ip_str_end : ip_str_start, addr );

    /* decrement IP address record */
    rc = modify_ip_range( ip_str_start, ip_str_end, collection_w, &error, take_from_end );
    switch( rc )
    {
        case 1: /* failed */
            syslog( LOG_ERR, "Error decrement IP address record" );
            break;
            /* TODO maybe we should crash here? */
        case 2: /* IP address pool ended */
            syslog( LOG_INFO, "IP address pool is empty, generate database" );
            generate_database( data );
    }
    json_object_put( jobj_root );

cleanup:
    bson_destroy( query );
    mongoc_collection_destroy( collection_w );
    mongoc_client_pool_push( pool, client );

    return res;
}

static int insert_ip_range( const char *start_address,
                            const char *end_address,
                            mongoc_collection_t *collection,
                            bson_error_t *error )
{
    bson_oid_t oid;
    bson_t *doc;

    doc = bson_new();
    bson_oid_init( &oid, NULL );
    BSON_APPEND_OID( doc, "_id", &oid );
    BSON_APPEND_UTF8( doc, "start", start_address );
    BSON_APPEND_UTF8( doc, "end", end_address );

    if( !mongoc_collection_insert( collection, MONGOC_INSERT_NONE,
                                   doc, NULL, error ))
    {
        bson_destroy( doc );
        return 1;
    }

    bson_destroy( doc );

    return 0;
}

static int modify_ip_range( const char *start_address,
                            const char *end_address,
                            mongoc_collection_t *collection,
                            bson_error_t *error,
                            int take_from_end )
{
    int res = 0;
    bson_t *doc;
    struct in6_addr ip_begin, ip_end;
    char ip_str_start[INET6_ADDRSTRLEN], ip_str_end[INET6_ADDRSTRLEN];

    memset(&ip_begin, 0, sizeof(ip_begin));
    memset(&ip_end, 0, sizeof(ip_end));
    memset(ip_str_start, 0, sizeof(ip_str_start));
    memset(ip_str_end, 0, sizeof(ip_str_end));

    doc = bson_new();

    inet_pton( AF_INET6, start_address, &ip_begin );
    inet_pton( AF_INET6, end_address, &ip_end );

    if( memcmp( ip_begin.s6_addr, ip_end.s6_addr, 16 ) == 0 )
    {
        res = 2;
        goto cleanup;
    }

    if( take_from_end )
    {
        decrement_ip_address( &ip_end );
        inet_ntop( AF_INET6, &ip_end, ip_str_end, INET6_ADDRSTRLEN );
        strncpy(ip_str_start, start_address, INET6_ADDRSTRLEN);
    }
    else
    {
        increment_ip_address( &ip_begin );
        inet_ntop( AF_INET6, &ip_begin, ip_str_start, INET6_ADDRSTRLEN );
        strncpy(ip_str_end, end_address, INET6_ADDRSTRLEN);
    }

    if( !mongoc_collection_remove( collection, MONGOC_REMOVE_SINGLE_REMOVE, doc, NULL, error ))
    {
        syslog( LOG_ERR, "mongoc_collection_remove error: %s", error->message );
        res = 1;
        goto cleanup;
    }

    if( insert_ip_range( ip_str_start, ip_str_end, collection, error ) != 0 )
    {
        syslog( LOG_ERR, "insert_ip_range error: %s", error->message );
        res = 1;
        goto cleanup;
    }

cleanup:
    bson_destroy( doc );

    return res;
}
