/* vim: set tabstop=4 shiftwidth=4 expandtab : */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <syslog.h>
#include <libgen.h>

#include <listen.h>
#include <read_config.h>
#include <manage_mongo_db.h>

int main( int argc, char **argv )
{
    int opt, flag, ret;
    FILE *fp;
    daemon_data_t data;
    daemon_config_t cfg;
    mongoc_client_pool_t *pool;
    mongoc_uri_t *uri;

    data.cfg = &cfg;

    strncpy( data.cfg_path, DEFAULT_CFG_PATH, PATH_MAX );

    openlog( basename( argv[0] ), 0, LOG_DAEMON );

    flag = 0;

    while(( opt = getopt( argc, argv, "ghnc:" )) != -1 )
    {
        switch( opt )
        {
            case 'g':
                flag = GENERATEDB;
                break;
            case 'n':
                flag = NODAEMON;
                break;
            case 'c':
                strncpy( data.cfg_path, optarg, PATH_MAX );
                break;
            case 'h':
            default:
                ERREXIT();
        }
    }

    if( read_config( &data ) != 0 )
    {
        fprintf( stderr, "Config file reading error\n" );
        exit( EXIT_FAILURE );
    }

    if( !( flag & NODAEMON ) && !( flag & GENERATEDB ))
    {
        printf( "Forking to background\n" );
        if( daemon(0, 0) != 0 )
        {
            fprintf( stderr, "detach failed: %s\n", strerror( errno ));
        }
    }
    syslog( LOG_INFO, "Daemon started" );

    mongoc_init();
    if(( uri = mongoc_uri_new( cfg.mongo_uri )) == NULL )
    {
        syslog( LOG_ERR, "Failed to parse a string containing a MongoDB style URI." );
        exit( EXIT_FAILURE );
    }
    pool = mongoc_client_pool_new( uri );
    data.pool = pool;

    if( flag & GENERATEDB )
    {
        printf( "Generate database...\n");
        if( generate_database( &data ) != 0 )
        {
            fprintf( stderr, "[Err]\n" );
            exit( EXIT_FAILURE );
        }
        printf( "[Ok]\n" );
        goto cleanup;
    }

    fp = fopen( DEFAULT_PID_PATH, "w" );
    if( fp == NULL )
    {
        syslog( LOG_WARNING, "Cannot create pid file %s : %s",
                DEFAULT_PID_PATH, strerror( errno ));
    }
    else
    {
        fprintf( fp, "%d", getpid());
        fclose( fp );
    }

    ret = wait_for_clients( &data );

cleanup:
    closelog();
    mongoc_uri_destroy( uri );
    mongoc_client_pool_destroy( pool );
    mongoc_cleanup();

    return ret;
}
