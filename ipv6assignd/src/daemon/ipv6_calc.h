/* vim: set tabstop=4 shiftwidth=4 expandtab : */
#ifndef IPV6_CALC_H_
#define IPV6_CALC_H_

#include <arpa/inet.h>

/**
 * @brief Decrement IPv6 address
 * param[in] addr IPv6 address pointer.
 */
void decrement_ip_address(struct in6_addr *addr);

/**
 * @brief Increment IPv6 address
 * param[in] addr IPv6 address pointer.
 */
void increment_ip_address(struct in6_addr *addr);

/**
 * @brief calculus of begin and end of subnet based on mask
 * param[in] start start IPv6 address pointer
 * param[in] end end IPv6 address pointer
 * param[in] end source IPv6 address pointer
 * param[in] mask network mask in CIDR notation
 */
void get_min_and_max_ip(struct in6_addr *start, struct in6_addr *end,
                        const struct in6_addr *address, int mask);

#endif /* IPV6_CALC_H_ */
