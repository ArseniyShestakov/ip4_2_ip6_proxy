/* vim: set tabstop=4 shiftwidth=4 expandtab : */
#ifndef MANAGE_MONGO_DB_H_
#define MANAGE_MONGO_DB_H_

#include <libmongoc-1.0/mongoc.h>

#include <read_config.h>

#define COLLECTION_DB_START "start_range"
#define COLLECTION_DB_WORK  "work_range"

int generate_database( const daemon_data_t * );

/** @brief Compare database start IP range with current IP range settings.
 *  If IP range was changed, then reload database.
 */
void reload_database( const daemon_data_t * );

int get_available_address( const daemon_data_t *, struct in6_addr * );

#endif /* MANAGE_MONGO_DB_H_ */
