/* vim: set tabstop=4 shiftwidth=4 expandtab : */
#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>
#include <uv.h>

#include <common.h>

void alloc_buffer( uv_handle_t *handle, size_t size, uv_buf_t *buf )
{
    ( void ) handle;
    *buf = uv_buf_init(( char * ) malloc( size ), size );
}

void on_close( uv_handle_t *peer )
{
    free( peer );
}

void on_write( uv_write_t *req, int status )
{
    write_req_t *wr;
    
    wr = ( write_req_t *) req;
    free( wr->buf.base );

    if( status < 0 )
    {
        syslog( LOG_WARNING, "Connection failed %s", uv_strerror( status));
        return;
    }
    if( !uv_is_closing((uv_handle_t *) req->handle ))
    {
        uv_close(( uv_handle_t * ) req->handle, on_close );
    }
    free( req );
}
