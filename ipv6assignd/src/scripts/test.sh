#!/bin/bash

DAEMON="../../../build/ipv6assignd/ipv6assignd"
CLIENT="../../../build/ipv6assignd/ipv6get"
DAEMONPIDFILE="/tmp/ipv6assignd.pid"

if ! test -f $DAEMON || ! test -f $CLIENT; then
  echo "run \"make ipv6assignd\" from \$git_root/build directory"
  exit 1
fi

CFGTCP=$(mktemp /tmp/ipv6cfg.XXXXXXXXXX)
CFGUNIX=$(mktemp /tmp/ipv6cfg.XXXXXXXXXX)
TESTS_OK=0

cat << EOF >> $CFGTCP
host = "127.0.0.1";
port = 12345;
mongodb_uri = "mongodb://localhost:27017";
mongodb_name = "ipv6assignd";
ipv6addr = ( { start = "2001::1"; end = "2001::5";  } );
EOF

cat << EOF >> $CFGUNIX
host = "/tmp/ipv6.sock";
mongodb_uri = "mongodb://localhost:27017";
mongodb_name = "ipv6assignd";
ipv6addr = ( { start = "2001::1"; end = "2001::5";  } );
EOF

clean_tmp_files()
{
  rm -f $CFGTCP $CFGUNIX
}

run_tests()
{
  local socktype="$1"
  local u_arg=""
  local test_num=2
    
  if [ "$socktype" = "unix" ]; then
    u_arg=" -u"
    test_num=14
  fi

  for i in $(seq 1 5); do
    echo "Testing address #${i} lease..."
    $CLIENT -l $u_arg | grep -qE "^Recieve address 2001::[0-5]"
    if [ "$?" -ne 0 ]; then
      echo "not ok ${test_num}"
      TESTS_OK=1
    else
      echo "ok ${test_num}"
    fi
    let test_num++
  done

  echo "Testing no address remain..."
  $CLIENT -l $u_arg 2>&1 | grep -qE "^No free ip addresses now$"
  if [ "$?" -ne 0 ]; then
    echo "not ok ${test_num}"
    TESTS_OK=1
  else
    echo "ok ${test_num}"
  fi
  let test_num++

  for i in $(seq 1 5); do
    echo "Testing address 2001::${i} release..."
    $CLIENT -r -a "2001::${i}" $u_arg
    $CLIENT -l $u_arg | grep -qE "^Recieve address 2001::${i}"
    if [ "$?" -ne 0 ]; then
      echo "not ok ${test_num}"
      TESTS_OK=1
    else
      echo "ok ${test_num}"
    fi
    let test_num++
  done
}

wait_for_daemon()
{
  local max_count=15
  local count=0

  while [ "$count" -ne "$max_count" ]; do
    if test -f $DAEMONPIDFILE; then
      break
    fi
    sleep 1
    let count++
  done

  if [ "$count" -eq "$max_count" ]; then
    echo "Daemon start failed"
    exit 1
  fi
}

recreate_db()
{
  ./clean_mongo_db.sh $CFGTCP
  $DAEMON -c $CFGTCP -g > /dev/null 2>&1
  if [ "$?" -ne 0 ]; then
    echo "Database generate false"
    exit 1
  fi
}

prepare_daemon_and_run_tests()
{
  local mode=$1
  if [ "$1" = "tcp" ]; then
    local cfg=$CFGTCP
    local test_num=1
  else
    local cfg=$CFGUNIX
    local test_num=13
  fi

  recreate_db > /dev/null
  echo "Try to run daemon..."
  $DAEMON -c $cfg > /dev/null 2>&1
  wait_for_daemon
  read DAEMONPID < $DAEMONPIDFILE
  if kill -0 $DAEMONPID >/dev/null 2>&1; then
    echo "ok ${test_num}"
  else
    echo -e "not ok ${test_num}\nDaemon was not started"
    exit 1
  fi

  run_tests $mode
  cat $DAEMONPIDFILE | xargs kill
}

trap clean_tmp_files exit
echo "1..24"
echo "Testing TCP listen mode."
prepare_daemon_and_run_tests tcp

echo -e "\nTesting UNIX domain listen mode."
prepare_daemon_and_run_tests unix

exit $TESTS_OK
