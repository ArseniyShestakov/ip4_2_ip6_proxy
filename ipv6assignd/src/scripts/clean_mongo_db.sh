#!/bin/sh

CFG="${1:-/etc/ipv6assignd.cfg}"

if ! test -r $CFG; then
    echo "Cannot read config file ${CFG}"
    exit 1
fi

URI=$(sed -r '/^mongodb_uri/!d;s/.*\/\/(.*)\";?/\1/' $CFG)
DB=$(sed -r '/^mongodb_name/!d;s/.*\"(.*)\";?/\1/' $CFG)

if grep -qE "^.*:.*@.*" <<< $URI; then
    USER=$(sed -r 's/^(\w+):.*/\1/' <<< $URI)
    PASS=$(sed -r 's/.*:(.*)@.*/\1/' <<< $URI)
    URI=$(sed -r 's/^.*@([^\/]+)(\/.*)?/\1/' <<< $URI)
    mongo ${URI}/${DB} -u $USER -p $PASS < cleandb.js
else
    mongo ${URI}/${DB} < cleandb.js
fi
